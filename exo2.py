# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 14:20:40 2020

@author: nassim.fouadh
"""


class SimpleCalculator:
    """
    Class calculatrice
    """
    @staticmethod
    def to_sum(number_1, number_2):
        """
        Methode pour calcul de somme
        Argument: fait la somme de "number_1" et "number_2"
        """
        return number_1 + number_2
    @staticmethod
    def substract(number_1, number_2):
        """
        Methode pour calcul de difference
        Argument: fait la soustraction entre "number_1" et "number_2"
        """
        return number_1 - number_2
    @staticmethod
    def multiply(number_1, number_2):
        """
	    Methode pour calcul de multiplication
	    Argument: fait la multiplication entre "number_1" et "number_2"
	    """
        return number_1 * number_2
    @staticmethod
    def divide(number_1, number_2):
        """
        Methode pour calcul de division
        Argument: fait la division entre "number_1" et "number_2"
        """
        return number_1 / number_2


if __name__ == "__main__":
    CALC = SimpleCalculator()
    print(CALC.to_sum(2, 2))
